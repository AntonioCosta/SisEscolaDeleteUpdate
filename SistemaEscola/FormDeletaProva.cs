﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormDeletaProva : Form
	{
		public FormDeletaProva()
		{
			InitializeComponent();
		}

		private void txtBuscarProva_TextChanged(object sender, EventArgs e)
		{
			gridResultadoProva.Rows.Clear();
			ProvaBD provaBD = new ProvaBD();
			List<Prova> provaBusca = provaBD.buscarProvasPorDescricao(txtBuscarProva.Text);

			gridResultadoProva.Visible = true;

			foreach (Prova p in provaBusca)
			{

				gridResultadoProva.Rows.Add(p.getIdProva(), p.getDescricao(), p.getDataProva());
			}
		}

		private void btApagarProva_Click(object sender, EventArgs e)
		{
			if (DialogResult.Yes == (MessageBox.Show("Deseja realmente excluir o registro", "Confirmação", MessageBoxButtons.YesNo)))
			{
				ProvaBD provaBD = new ProvaBD();
				Prova prova = new Prova();

				prova.setIdProva(Convert.ToInt16(gridResultadoProva.CurrentRow.Cells[0].Value.ToString()));
				int result = provaBD.apagarProva(prova);
				if (result == 0)
				{
					MessageBox.Show("Erro ao apagar cadastro de Prova");
				}
				else
				{
					MessageBox.Show("Cadastro de Prova apagado com sucesso", "Sucesso", MessageBoxButtons.OK);
				}

				//Atualiza o grid novamente
				gridResultadoProva.Rows.Clear();
				List<Prova> provaBusca = provaBD.buscarProvasPorDescricao(txtBuscarProva.Text);

				gridResultadoProva.Visible = true;

				foreach (Prova p in provaBusca)
				{

					gridResultadoProva.Rows.Add(p.getIdProva(), p.getDescricao(), p.getDataProva());
				}
			}
		}
	}
}
