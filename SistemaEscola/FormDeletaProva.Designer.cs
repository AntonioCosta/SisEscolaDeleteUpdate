﻿namespace SistemaEscola
{
	partial class FormDeletaProva
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gridResultadoProva = new System.Windows.Forms.DataGridView();
			this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Prova = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtBuscarProva = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.btApagarProva = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoProva)).BeginInit();
			this.SuspendLayout();
			// 
			// gridResultadoProva
			// 
			this.gridResultadoProva.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridResultadoProva.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Prova});
			this.gridResultadoProva.Location = new System.Drawing.Point(2, 70);
			this.gridResultadoProva.Name = "gridResultadoProva";
			this.gridResultadoProva.Size = new System.Drawing.Size(350, 211);
			this.gridResultadoProva.TabIndex = 5;
			this.gridResultadoProva.Visible = false;
			// 
			// ID
			// 
			this.ID.HeaderText = "ID";
			this.ID.Name = "ID";
			this.ID.Width = 30;
			// 
			// Prova
			// 
			this.Prova.HeaderText = "Prova";
			this.Prova.Name = "Prova";
			this.Prova.Width = 275;
			// 
			// txtBuscarProva
			// 
			this.txtBuscarProva.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtBuscarProva.Location = new System.Drawing.Point(52, 13);
			this.txtBuscarProva.Name = "txtBuscarProva";
			this.txtBuscarProva.Size = new System.Drawing.Size(241, 20);
			this.txtBuscarProva.TabIndex = 4;
			this.txtBuscarProva.TextChanged += new System.EventHandler(this.txtBuscarProva_TextChanged);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(191, 309);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 12;
			this.button1.Text = "Cancelar";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// btApagarProva
			// 
			this.btApagarProva.Location = new System.Drawing.Point(82, 309);
			this.btApagarProva.Name = "btApagarProva";
			this.btApagarProva.Size = new System.Drawing.Size(75, 23);
			this.btApagarProva.TabIndex = 11;
			this.btApagarProva.Text = "Apagar";
			this.btApagarProva.UseVisualStyleBackColor = true;
			this.btApagarProva.Click += new System.EventHandler(this.btApagarProva_Click);
			// 
			// FormDeletaProva
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(354, 348);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.btApagarProva);
			this.Controls.Add(this.gridResultadoProva);
			this.Controls.Add(this.txtBuscarProva);
			this.Name = "FormDeletaProva";
			this.Text = "FormDeletaProva";
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoProva)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView gridResultadoProva;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID;
		private System.Windows.Forms.DataGridViewTextBoxColumn Prova;
		private System.Windows.Forms.TextBox txtBuscarProva;
		public System.Windows.Forms.Button button1;
		public System.Windows.Forms.Button btApagarProva;
	}
}