﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormMenu : Form
	{
		//List<Aluno> alunoB = new List<Aluno>();

		public FormMenu()
		{
			InitializeComponent();
			//this.alunoB = alunoBusca;
		}
		private void alunoToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}
		private void editarCadastroAlunoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			AlunoBD alunoBD = new AlunoBD();
			List<Aluno> alunoBusca = alunoBD.buscarAlunosPorNome(txtAluno.Text);


			FormAluno formA = new FormAluno();
			formA.Show();
			formA.lbl0ou1.Text = "1";  //se for 0 está salvando um novo(insert) aluno, se for 1 está editando(update)

			foreach (Aluno a in alunoBusca)
			{
				if (a.getIdAluno() == Convert.ToInt16(lblid.Text))
				{
					formA.lblidA.Text = Convert.ToString(a.getIdAluno());
					formA.txtNome.Text = a.getNome();
					formA.txtDataNasc.Text = Convert.ToString(a.getDataNasc());
					formA.txtMatricula.Text = Convert.ToString(a.getMatricula());
					formA.txtDataMatricula.Text = Convert.ToString(a.getDataMatricula());
					break;
				}
				//gridResultadoAluno.Rows.Add(a.getIdAluno(), a.getNome(), a.getMatricula(), a.getDataNasc(), a.getDataMatricula());
			}

			//formA.txtNome.Text = txtAluno.Text;
		}

		private void apagarDadosToolStripMenuItem_Click(object sender, EventArgs e)
		{
	
		}

		private void disciplinaToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void editarCadastroDeNotaDoAlunoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NotaBD notaBD = new NotaBD();
			List<Nota> notasBusca = notaBD.buscarNotasPorNomeAluno(txtAluno.Text);


			foreach (Nota n in notasBusca)
			{
				if (n.getAluno().getIdAluno() == Convert.ToInt16(lblid.Text))
				{
					Aluno a = n.getAluno();
					Prova p = n.getProva();

					FormNota formN = new FormNota(a, p);
					formN.Show();

					formN.lbl0ou1.Text = "1";
					formN.lbl.Text = Convert.ToString(n.getAluno().getIdAluno());
					formN.txtAluno.Text = n.getAluno().getNome();
					formN.txtProva.Text = n.getProva().getDescricao();
					formN.txtNota.Text = Convert.ToString(n.getNota());

					//formN.txtAluno.Enabled = true;
					//formN.txtProva.Enabled = true;
					break;
				}
			}
		}

		private void registroDoAlunoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (DialogResult.Yes == (MessageBox.Show("Deseja realmente excluir o registro", "Confirmação", MessageBoxButtons.YesNo)))
			{
				AlunoBD alunoBD = new AlunoBD();
				Aluno aluno = new Aluno();

				aluno.setIdAluno(Convert.ToInt16(lblid.Text));
				int result = alunoBD.apagarAluno(aluno);
				if (result == 0)
				{
					MessageBox.Show("Erro ao apagar cadastro de aluno na Tabela Aluno");
				}
				else
				{
					MessageBox.Show("Cadastro de aluno apagado com sucesso", "Sucesso", MessageBoxButtons.OK);
					//txtAluno.Text = "";
					this.Close();
				}

			}
		}

		private void somenteNotaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NotaBD notaBD = new NotaBD();
			List<Nota> notasBusca = notaBD.buscarNotasPorNomeAluno(txtAluno.Text);


			foreach (Nota n in notasBusca)
			{
				if (n.getAluno().getIdAluno() == Convert.ToInt16(lblid.Text))
				{
					Aluno a = n.getAluno();
					Prova p = n.getProva();

					FormNota formN = new FormNota(a, p);
					formN.Show();

					formN.lbl0ou1.Text = "2";
					formN.lbl.Text = Convert.ToString(n.getAluno().getIdAluno());
					formN.btSalvarNota.Text = "Apagar";
					formN.txtAluno.Text = n.getAluno().getNome();
					formN.txtProva.Text = n.getProva().getDescricao();
					formN.txtNota.Text = Convert.ToString(n.getNota());

					//formN.txtAluno.Enabled = true;
					//formN.txtProva.Enabled = true;
					break;
				}
			}
		}
	}
}
