﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormDeletaAluno : Form
	{
		public FormDeletaAluno()
		{
			InitializeComponent();
		}

		private void txtBuscarAluno_TextChanged(object sender, EventArgs e)
		{
			gridResultadoAluno.Rows.Clear();
			AlunoBD alunoBD = new AlunoBD();
			List<Aluno> alunoBusca = alunoBD.buscarAlunosPorNome(txtBuscarAluno.Text);

			gridResultadoAluno.Visible = true;

			foreach (Aluno a in alunoBusca)
			{

				gridResultadoAluno.Rows.Add(a.getIdAluno(), a.getNome(), a.getMatricula(), a.getDataNasc(), a.getDataMatricula());
			}
		}

		private void btApagarAluno_Click(object sender, EventArgs e)
		{
			if(DialogResult.Yes==(MessageBox.Show("Deseja realmente excluir o registro", "Confirmação", MessageBoxButtons.YesNo)))
			{
				AlunoBD alunoBD = new AlunoBD();
				Aluno aluno = new Aluno();

				aluno.setIdAluno(Convert.ToInt16(gridResultadoAluno.CurrentRow.Cells[0].Value.ToString()));
				int result = alunoBD.apagarAluno(aluno);
				if (result == 0)
				{
					MessageBox.Show("Erro ao apagar cadastro de aluno na Tabela Aluno");
				}
				else
				{
					MessageBox.Show("Cadastro de aluno apagado com sucesso", "Sucesso", MessageBoxButtons.OK);
				}

				//Atualiza o grid novamente
				gridResultadoAluno.Rows.Clear();
				List<Aluno> alunoBusca = alunoBD.buscarAlunosPorNome(txtBuscarAluno.Text);

				gridResultadoAluno.Visible = true;

				foreach (Aluno a in alunoBusca)
				{

					gridResultadoAluno.Rows.Add(a.getIdAluno(), a.getNome(), a.getMatricula(), a.getDataNasc(), a.getDataMatricula());
				}
			}
		}

		private void gridResultadoAluno_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{

		}
	}
}
