﻿namespace SistemaEscola
{
	partial class FormPrincipal
	{
		/// <summary>
		/// Variável de designer necessária.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpar os recursos que estão sendo usados.
		/// </summary>
		/// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código gerado pelo Windows Form Designer

		/// <summary>
		/// Método necessário para suporte ao Designer - não modifique 
		/// o conteúdo deste método com o editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.novoAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buscarAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buscarPorDescriçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buscarAlunoFormDeletaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.disciplinaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cadastrarProvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.apagarRegistroDeProvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.notaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.registarNotaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBoxAlunos = new System.Windows.Forms.GroupBox();
			this.lblErro = new System.Windows.Forms.Label();
			this.lbl = new System.Windows.Forms.Label();
			this.txtDataProva = new System.Windows.Forms.DateTimePicker();
			this.lblDataProva = new System.Windows.Forms.Label();
			this.txtDescricao = new System.Windows.Forms.TextBox();
			this.lblDescricao = new System.Windows.Forms.Label();
			this.lblNenhumRegistro = new System.Windows.Forms.Label();
			this.btBuscaAluno = new System.Windows.Forms.Button();
			this.btLimparBusca = new System.Windows.Forms.Button();
			this.gridAluno = new System.Windows.Forms.DataGridView();
			this.IDaluno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colunaNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.IDprova = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colunaProva = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colunaNota = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtNomeAluno = new System.Windows.Forms.TextBox();
			this.lblNomeAluno = new System.Windows.Forms.Label();
			this.btVoltar = new System.Windows.Forms.Button();
			this.menuStrip1.SuspendLayout();
			this.groupBoxAlunos.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridAluno)).BeginInit();
			this.SuspendLayout();
			// 
			// alunoToolStripMenuItem
			// 
			this.alunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoAlunoToolStripMenuItem,
            this.buscarAlunoToolStripMenuItem,
            this.buscarPorDescriçãoToolStripMenuItem,
            this.buscarAlunoFormDeletaToolStripMenuItem});
			this.alunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
			this.alunoToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.alunoToolStripMenuItem.Size = new System.Drawing.Size(113, 45);
			this.alunoToolStripMenuItem.Text = "Aluno";
			// 
			// novoAlunoToolStripMenuItem
			// 
			this.novoAlunoToolStripMenuItem.AutoToolTip = true;
			this.novoAlunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.novoAlunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.novoAlunoToolStripMenuItem.Name = "novoAlunoToolStripMenuItem";
			this.novoAlunoToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.novoAlunoToolStripMenuItem.Text = "Matricular Aluno";
			this.novoAlunoToolStripMenuItem.Click += new System.EventHandler(this.novoAlunoToolStripMenuItem_Click);
			// 
			// buscarAlunoToolStripMenuItem
			// 
			this.buscarAlunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.buscarAlunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.buscarAlunoToolStripMenuItem.Name = "buscarAlunoToolStripMenuItem";
			this.buscarAlunoToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.buscarAlunoToolStripMenuItem.Text = "Buscar Aluno";
			this.buscarAlunoToolStripMenuItem.Click += new System.EventHandler(this.buscarAlunoToolStripMenuItem_Click);
			// 
			// buscarPorDescriçãoToolStripMenuItem
			// 
			this.buscarPorDescriçãoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.buscarPorDescriçãoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buscarPorDescriçãoToolStripMenuItem.Name = "buscarPorDescriçãoToolStripMenuItem";
			this.buscarPorDescriçãoToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.buscarPorDescriçãoToolStripMenuItem.Text = "Buscar por Descrição";
			this.buscarPorDescriçãoToolStripMenuItem.Click += new System.EventHandler(this.buscarPorDescriçãoToolStripMenuItem_Click);
			// 
			// buscarAlunoFormDeletaToolStripMenuItem
			// 
			this.buscarAlunoFormDeletaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.buscarAlunoFormDeletaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buscarAlunoFormDeletaToolStripMenuItem.Name = "buscarAlunoFormDeletaToolStripMenuItem";
			this.buscarAlunoFormDeletaToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.buscarAlunoFormDeletaToolStripMenuItem.Text = "Apagar registro de Aluno";
			this.buscarAlunoFormDeletaToolStripMenuItem.Click += new System.EventHandler(this.buscarAlunoFormDeletaToolStripMenuItem_Click);
			// 
			// disciplinaToolStripMenuItem
			// 
			this.disciplinaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.disciplinaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarProvaToolStripMenuItem,
            this.apagarRegistroDeProvaToolStripMenuItem});
			this.disciplinaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.disciplinaToolStripMenuItem.Name = "disciplinaToolStripMenuItem";
			this.disciplinaToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.disciplinaToolStripMenuItem.Size = new System.Drawing.Size(113, 45);
			this.disciplinaToolStripMenuItem.Text = "Prova";
			// 
			// cadastrarProvaToolStripMenuItem
			// 
			this.cadastrarProvaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.cadastrarProvaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.cadastrarProvaToolStripMenuItem.Name = "cadastrarProvaToolStripMenuItem";
			this.cadastrarProvaToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
			this.cadastrarProvaToolStripMenuItem.Text = "Cadastrar Prova";
			this.cadastrarProvaToolStripMenuItem.Click += new System.EventHandler(this.cadastrarProvaToolStripMenuItem_Click);
			// 
			// apagarRegistroDeProvaToolStripMenuItem
			// 
			this.apagarRegistroDeProvaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.apagarRegistroDeProvaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.apagarRegistroDeProvaToolStripMenuItem.Name = "apagarRegistroDeProvaToolStripMenuItem";
			this.apagarRegistroDeProvaToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
			this.apagarRegistroDeProvaToolStripMenuItem.Text = "Apagar registro de Prova";
			this.apagarRegistroDeProvaToolStripMenuItem.Click += new System.EventHandler(this.apagarRegistroDeProvaToolStripMenuItem_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.disciplinaToolStripMenuItem,
            this.notaToolStripMenuItem});
			this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(220, 100, 220, 100);
			this.menuStrip1.Size = new System.Drawing.Size(554, 369);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
			// 
			// notaToolStripMenuItem
			// 
			this.notaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.notaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registarNotaToolStripMenuItem});
			this.notaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.notaToolStripMenuItem.Name = "notaToolStripMenuItem";
			this.notaToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.notaToolStripMenuItem.Size = new System.Drawing.Size(113, 45);
			this.notaToolStripMenuItem.Text = "Nota";
			// 
			// registarNotaToolStripMenuItem
			// 
			this.registarNotaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.registarNotaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.registarNotaToolStripMenuItem.Name = "registarNotaToolStripMenuItem";
			this.registarNotaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.registarNotaToolStripMenuItem.Text = "Registar Nota";
			this.registarNotaToolStripMenuItem.Click += new System.EventHandler(this.registarNotaToolStripMenuItem_Click);
			// 
			// groupBoxAlunos
			// 
			this.groupBoxAlunos.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.groupBoxAlunos.Controls.Add(this.lblErro);
			this.groupBoxAlunos.Controls.Add(this.lbl);
			this.groupBoxAlunos.Controls.Add(this.txtDataProva);
			this.groupBoxAlunos.Controls.Add(this.lblDataProva);
			this.groupBoxAlunos.Controls.Add(this.txtDescricao);
			this.groupBoxAlunos.Controls.Add(this.lblDescricao);
			this.groupBoxAlunos.Controls.Add(this.lblNenhumRegistro);
			this.groupBoxAlunos.Controls.Add(this.btBuscaAluno);
			this.groupBoxAlunos.Controls.Add(this.btLimparBusca);
			this.groupBoxAlunos.Controls.Add(this.gridAluno);
			this.groupBoxAlunos.Controls.Add(this.txtNomeAluno);
			this.groupBoxAlunos.Controls.Add(this.lblNomeAluno);
			this.groupBoxAlunos.Location = new System.Drawing.Point(0, 41);
			this.groupBoxAlunos.Name = "groupBoxAlunos";
			this.groupBoxAlunos.Size = new System.Drawing.Size(554, 328);
			this.groupBoxAlunos.TabIndex = 2;
			this.groupBoxAlunos.TabStop = false;
			this.groupBoxAlunos.Text = "Buscar Alunos";
			this.groupBoxAlunos.Visible = false;
			// 
			// lblErro
			// 
			this.lblErro.AutoSize = true;
			this.lblErro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblErro.ForeColor = System.Drawing.Color.Red;
			this.lblErro.Location = new System.Drawing.Point(48, 147);
			this.lblErro.Name = "lblErro";
			this.lblErro.Size = new System.Drawing.Size(460, 20);
			this.lblErro.TabIndex = 22;
			this.lblErro.Text = "Descrição ou Data incorreta ou não existe nos registros!";
			this.lblErro.Visible = false;
			// 
			// lbl
			// 
			this.lbl.AutoSize = true;
			this.lbl.Location = new System.Drawing.Point(507, 20);
			this.lbl.Name = "lbl";
			this.lbl.Size = new System.Drawing.Size(35, 13);
			this.lbl.TabIndex = 21;
			this.lbl.Text = "label1";
			this.lbl.Visible = false;
			// 
			// txtDataProva
			// 
			this.txtDataProva.Location = new System.Drawing.Point(159, 64);
			this.txtDataProva.Name = "txtDataProva";
			this.txtDataProva.Size = new System.Drawing.Size(173, 20);
			this.txtDataProva.TabIndex = 20;
			// 
			// lblDataProva
			// 
			this.lblDataProva.AutoSize = true;
			this.lblDataProva.Location = new System.Drawing.Point(59, 70);
			this.lblDataProva.Name = "lblDataProva";
			this.lblDataProva.Size = new System.Drawing.Size(104, 13);
			this.lblDataProva.TabIndex = 19;
			this.lblDataProva.Text = "Data de Realização:";
			// 
			// txtDescricao
			// 
			this.txtDescricao.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtDescricao.Location = new System.Drawing.Point(123, 38);
			this.txtDescricao.Name = "txtDescricao";
			this.txtDescricao.Size = new System.Drawing.Size(347, 20);
			this.txtDescricao.TabIndex = 7;
			this.txtDescricao.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// lblDescricao
			// 
			this.lblDescricao.AutoSize = true;
			this.lblDescricao.Location = new System.Drawing.Point(59, 41);
			this.lblDescricao.Name = "lblDescricao";
			this.lblDescricao.Size = new System.Drawing.Size(58, 13);
			this.lblDescricao.TabIndex = 6;
			this.lblDescricao.Text = "Descriçao:";
			this.lblDescricao.Click += new System.EventHandler(this.label2_Click);
			// 
			// lblNenhumRegistro
			// 
			this.lblNenhumRegistro.AutoSize = true;
			this.lblNenhumRegistro.ForeColor = System.Drawing.Color.Red;
			this.lblNenhumRegistro.Location = new System.Drawing.Point(103, 99);
			this.lblNenhumRegistro.Name = "lblNenhumRegistro";
			this.lblNenhumRegistro.Size = new System.Drawing.Size(144, 13);
			this.lblNenhumRegistro.TabIndex = 5;
			this.lblNenhumRegistro.Text = "Nenhum registro encontrado!";
			this.lblNenhumRegistro.Visible = false;
			// 
			// btBuscaAluno
			// 
			this.btBuscaAluno.Location = new System.Drawing.Point(395, 94);
			this.btBuscaAluno.Name = "btBuscaAluno";
			this.btBuscaAluno.Size = new System.Drawing.Size(75, 23);
			this.btBuscaAluno.TabIndex = 4;
			this.btBuscaAluno.Text = "Buscar";
			this.btBuscaAluno.UseVisualStyleBackColor = true;
			this.btBuscaAluno.Click += new System.EventHandler(this.btBuscaAluno_Click);
			// 
			// btLimparBusca
			// 
			this.btLimparBusca.Location = new System.Drawing.Point(304, 94);
			this.btLimparBusca.Name = "btLimparBusca";
			this.btLimparBusca.Size = new System.Drawing.Size(75, 23);
			this.btLimparBusca.TabIndex = 3;
			this.btLimparBusca.Text = "Limpar";
			this.btLimparBusca.UseVisualStyleBackColor = true;
			this.btLimparBusca.Click += new System.EventHandler(this.btLimparBusca_Click);
			// 
			// gridAluno
			// 
			this.gridAluno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridAluno.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDaluno,
            this.colunaNome,
            this.IDprova,
            this.colunaProva,
            this.colunaNota});
			this.gridAluno.Location = new System.Drawing.Point(6, 123);
			this.gridAluno.Name = "gridAluno";
			this.gridAluno.Size = new System.Drawing.Size(542, 198);
			this.gridAluno.TabIndex = 2;
			this.gridAluno.Visible = false;
			this.gridAluno.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridAluno_CellContentClick);
			// 
			// IDaluno
			// 
			this.IDaluno.HeaderText = "ID";
			this.IDaluno.Name = "IDaluno";
			this.IDaluno.Width = 25;
			// 
			// colunaNome
			// 
			this.colunaNome.HeaderText = "Nome";
			this.colunaNome.Name = "colunaNome";
			this.colunaNome.Width = 268;
			// 
			// IDprova
			// 
			this.IDprova.HeaderText = "ID";
			this.IDprova.Name = "IDprova";
			this.IDprova.Width = 25;
			// 
			// colunaProva
			// 
			this.colunaProva.HeaderText = "Prova";
			this.colunaProva.Name = "colunaProva";
			this.colunaProva.Width = 130;
			// 
			// colunaNota
			// 
			this.colunaNota.HeaderText = "Nota";
			this.colunaNota.Name = "colunaNota";
			this.colunaNota.Width = 50;
			// 
			// txtNomeAluno
			// 
			this.txtNomeAluno.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtNomeAluno.Location = new System.Drawing.Point(103, 26);
			this.txtNomeAluno.Name = "txtNomeAluno";
			this.txtNomeAluno.Size = new System.Drawing.Size(367, 20);
			this.txtNomeAluno.TabIndex = 1;
			this.txtNomeAluno.TextChanged += new System.EventHandler(this.txtBuscaAluno_TextChanged);
			// 
			// lblNomeAluno
			// 
			this.lblNomeAluno.AutoSize = true;
			this.lblNomeAluno.Location = new System.Drawing.Point(59, 29);
			this.lblNomeAluno.Name = "lblNomeAluno";
			this.lblNomeAluno.Size = new System.Drawing.Size(38, 13);
			this.lblNomeAluno.TabIndex = 0;
			this.lblNomeAluno.Text = "Nome:";
			// 
			// btVoltar
			// 
			this.btVoltar.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.btVoltar.ForeColor = System.Drawing.Color.Red;
			this.btVoltar.Location = new System.Drawing.Point(0, 0);
			this.btVoltar.Name = "btVoltar";
			this.btVoltar.Size = new System.Drawing.Size(51, 23);
			this.btVoltar.TabIndex = 3;
			this.btVoltar.Text = "Voltar";
			this.btVoltar.UseVisualStyleBackColor = false;
			this.btVoltar.Click += new System.EventHandler(this.btVoltar_Click);
			// 
			// FormPrincipal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(554, 369);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.groupBoxAlunos);
			this.Controls.Add(this.btVoltar);
			this.Name = "FormPrincipal";
			this.Text = "Tela Inicial";
			this.Load += new System.EventHandler(this.FormPrincipal_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.groupBoxAlunos.ResumeLayout(false);
			this.groupBoxAlunos.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridAluno)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem novoAlunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem buscarAlunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem disciplinaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cadastrarProvaToolStripMenuItem;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem notaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem registarNotaToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBoxAlunos;
		private System.Windows.Forms.Label lblNenhumRegistro;
		private System.Windows.Forms.Button btBuscaAluno;
		private System.Windows.Forms.Button btLimparBusca;
		private System.Windows.Forms.DataGridView gridAluno;
		private System.Windows.Forms.TextBox txtNomeAluno;
		private System.Windows.Forms.Label lblNomeAluno;
		private System.Windows.Forms.Button btVoltar;
		private System.Windows.Forms.DataGridViewTextBoxColumn IDaluno;
		private System.Windows.Forms.DataGridViewTextBoxColumn colunaNome;
		private System.Windows.Forms.DataGridViewTextBoxColumn IDprova;
		private System.Windows.Forms.DataGridViewTextBoxColumn colunaProva;
		private System.Windows.Forms.DataGridViewTextBoxColumn colunaNota;
		private System.Windows.Forms.ToolStripMenuItem buscarPorDescriçãoToolStripMenuItem;
		private System.Windows.Forms.TextBox txtDescricao;
		private System.Windows.Forms.Label lblDescricao;
		private System.Windows.Forms.DateTimePicker txtDataProva;
		private System.Windows.Forms.Label lblDataProva;
		private System.Windows.Forms.Label lbl;
		private System.Windows.Forms.Label lblErro;
		private System.Windows.Forms.ToolStripMenuItem buscarAlunoFormDeletaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem apagarRegistroDeProvaToolStripMenuItem;
	}
}

