﻿namespace SistemaEscola
{
	partial class FormAluno
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtNome = new System.Windows.Forms.TextBox();
			this.txtMatricula = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btSalvarAluno = new System.Windows.Forms.Button();
			this.txtDataNasc = new System.Windows.Forms.DateTimePicker();
			this.txtDataMatricula = new System.Windows.Forms.DateTimePicker();
			this.lbl0ou1 = new System.Windows.Forms.Label();
			this.lblidA = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(24, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(85, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nome Completo:";
			// 
			// txtNome
			// 
			this.txtNome.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtNome.Location = new System.Drawing.Point(105, 47);
			this.txtNome.Name = "txtNome";
			this.txtNome.Size = new System.Drawing.Size(261, 20);
			this.txtNome.TabIndex = 1;
			this.txtNome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
			// 
			// txtMatricula
			// 
			this.txtMatricula.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtMatricula.Location = new System.Drawing.Point(193, 127);
			this.txtMatricula.Name = "txtMatricula";
			this.txtMatricula.Size = new System.Drawing.Size(173, 20);
			this.txtMatricula.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(124, 130);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Nº Matrícula:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(86, 85);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(107, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Data de Nascimento:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(97, 166);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "Data da Matrícula:";
			// 
			// btSalvarAluno
			// 
			this.btSalvarAluno.Location = new System.Drawing.Point(170, 244);
			this.btSalvarAluno.Name = "btSalvarAluno";
			this.btSalvarAluno.Size = new System.Drawing.Size(75, 23);
			this.btSalvarAluno.TabIndex = 8;
			this.btSalvarAluno.Text = "Salvar";
			this.btSalvarAluno.UseVisualStyleBackColor = true;
			this.btSalvarAluno.Click += new System.EventHandler(this.btSalvarAluno_Click);
			// 
			// txtDataNasc
			// 
			this.txtDataNasc.Location = new System.Drawing.Point(193, 79);
			this.txtDataNasc.Name = "txtDataNasc";
			this.txtDataNasc.Size = new System.Drawing.Size(173, 20);
			this.txtDataNasc.TabIndex = 9;
			// 
			// txtDataMatricula
			// 
			this.txtDataMatricula.Location = new System.Drawing.Point(193, 159);
			this.txtDataMatricula.Name = "txtDataMatricula";
			this.txtDataMatricula.Size = new System.Drawing.Size(173, 20);
			this.txtDataMatricula.TabIndex = 10;
			// 
			// lbl0ou1
			// 
			this.lbl0ou1.AutoSize = true;
			this.lbl0ou1.Location = new System.Drawing.Point(395, -1);
			this.lbl0ou1.Name = "lbl0ou1";
			this.lbl0ou1.Size = new System.Drawing.Size(13, 13);
			this.lbl0ou1.TabIndex = 11;
			this.lbl0ou1.Text = "0";
			this.lbl0ou1.Visible = false;
			this.lbl0ou1.Click += new System.EventHandler(this.lbl0ou1_Click);
			// 
			// lblidA
			// 
			this.lblidA.AutoSize = true;
			this.lblidA.Location = new System.Drawing.Point(0, -1);
			this.lblidA.Name = "lblidA";
			this.lblidA.Size = new System.Drawing.Size(15, 13);
			this.lblidA.TabIndex = 12;
			this.lblidA.Text = "id";
			this.lblidA.Visible = false;
			// 
			// FormAluno
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(412, 344);
			this.Controls.Add(this.lblidA);
			this.Controls.Add(this.lbl0ou1);
			this.Controls.Add(this.txtDataMatricula);
			this.Controls.Add(this.txtDataNasc);
			this.Controls.Add(this.btSalvarAluno);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtMatricula);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtNome);
			this.Controls.Add(this.label1);
			this.Name = "FormAluno";
			this.Text = "Matricula do Aluno";
			this.Load += new System.EventHandler(this.FormAluno_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.TextBox txtNome;
		public System.Windows.Forms.TextBox txtMatricula;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		public System.Windows.Forms.Button btSalvarAluno;
		public System.Windows.Forms.DateTimePicker txtDataNasc;
		public System.Windows.Forms.DateTimePicker txtDataMatricula;
		public System.Windows.Forms.Label lbl0ou1;
		public System.Windows.Forms.Label lblidA;
	}
}