﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormPrincipal : Form
	{
		List<Aluno> alunos;
		List<Prova> provas;
		List<Nota> notas;

		Aluno alunoSelecionado;
		Prova provaSelecionada;

		Aluno aluno = new Aluno();
		Prova prova = new Prova();

		

		public FormPrincipal()
		{
			InitializeComponent();

			alunoSelecionado = new Aluno();
			provaSelecionada = new Prova();

			alunos = new List<Aluno>();
			provas = new List<Prova>();
			notas = new List<Nota>();
			Conexão c = new Conexão();
			c.conecta();
		}

		private void novoAlunoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormAluno formAluno = new FormAluno();
			formAluno.Show();
		}

		private void cadastrarProvaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormProva formProva = new FormProva(provas);
			formProva.Show();
		}

		private void registarNotaToolStripMenuItem_Click(object sender, EventArgs e)
		{        // coloquei alunos no lugar de notas
			FormNota formNota = new FormNota(alunoSelecionado, provaSelecionada);
			formNota.Show();
		}

		private void FormPrincipal_Load(object sender, EventArgs e)
		{

		}

		private void btBuscaAluno_Click(object sender, EventArgs e)
		{
			gridAluno.Rows.Clear();
			NotaBD notaBD = new NotaBD();
			List<Nota> notasBusca;

			if (lbl.Text == "1")
			{
				notasBusca = notaBD.buscarNotasPorNomeAluno(txtNomeAluno.Text);
			}
			else
			{
				DateTime data = Convert.ToDateTime(txtDataProva.Text);
				notasBusca = notaBD.buscarNotasPorDescricao(txtDescricao.Text, data);
			}
			
			/*foreach (Nota n in notas)
			{
				if (n.getAluno().getNome().Contains(txtBuscaAluno.Text))
				{
					notasBusca.Add(n);
				}
			}*/

			if (notasBusca.Count() == 0) //serve para as duas formas de busca
			{
				lblNenhumRegistro.Visible = true;
				gridAluno.Visible = false;
				if(lbl.Text == "2")
				{
					lblErro.Visible = true;
					lblNenhumRegistro.Visible = false;
				}
			}
			else
			{
				gridAluno.Visible = true;
				lblNenhumRegistro.Visible = false;
				lblErro.Visible = false;

				foreach (Nota n in notasBusca)
				{
					gridAluno.Rows.Add(n.getAluno().getIdAluno(), n.getAluno().getNome(), n.getProva().getIdProva(), n.getProva().getDescricao(), n.getNota());
				}
			}

		}

		private void btLimparBusca_Click(object sender, EventArgs e)
		{
			txtNomeAluno.Text = "";
			gridAluno.Rows.Clear();
			lblNenhumRegistro.Visible = false;
		}

		private void buscarAlunoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			lbl.Text = "1";

			groupBoxAlunos.Visible = true;

			lblNomeAluno.Visible = true;
			txtNomeAluno.Visible = true;

			lblDescricao.Visible = false;
			txtDescricao.Visible = false;

			lblDataProva.Visible = false;
			txtDataProva.Visible = false;

			btVoltar.Visible = true;
			menuStrip1.Visible = false;
			gridAluno.Visible = false;
		}

		private void btVoltar_Click(object sender, EventArgs e)
		{
			groupBoxAlunos.Visible = false;
			btVoltar.Visible = false;
			menuStrip1.Visible = true;
		}

		private void txtBuscaAluno_TextChanged(object sender, EventArgs e)
		{

		}

		private void gridAluno_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{/*
			// pq o parametro eh alunos??
			FormAluno formA = new FormAluno(alunos);
			formA.Show();
			formA.txtNome.Text = gridAluno.CurrentRow.Cells[1].Value.ToString();
			// alterei txtNome de private pra public */

			FormMenu formMenu = new FormMenu();
			formMenu.Show();

			formMenu.lblid.Text = gridAluno.CurrentRow.Cells[0].Value.ToString();
			formMenu.txtAluno.Text = gridAluno.CurrentRow.Cells[1].Value.ToString();

		}

		private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{

		}

		private void buscarPorDescriçãoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			lbl.Text = "2";

			groupBoxAlunos.Visible = true;

			lblDescricao.Visible = true;
			txtDescricao.Visible = true;

			lblDataProva.Visible = true;
			txtDataProva.Visible = true;

			lblNomeAluno.Visible = false;
			txtNomeAluno.Visible = false;

			btVoltar.Visible = true;
			menuStrip1.Visible = false;
			gridAluno.Visible = false;
		}

		private void label2_Click(object sender, EventArgs e)
		{

		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}

		private void buscarAlunoFormDeletaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormDeletaAluno formDeletaAluno = new FormDeletaAluno();
			formDeletaAluno.Show();
		}

		private void apagarRegistroDeProvaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormDeletaProva formDeletaProva = new FormDeletaProva();
			formDeletaProva.Show();
		}
	}
}
