﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormNota : Form
	{
		Aluno alunoSelecionado;
		Prova provaSelecionada;

		Aluno novoAluno;
		Prova novaProva;
		//Nota novaNota;
		//List<Aluno> alunos;
		//List<Prova> provas;
		//List<Nota> notas;

		public FormNota(Aluno alunoSelecionado, Prova provaSelecionada)
		{
			InitializeComponent();

			this.alunoSelecionado = new Aluno();
			this.provaSelecionada = new Prova();
			//this.alunos = alunos;
			//this.provas = provas;
			//this.notas = notas;
			novoAluno = new Aluno();
			novaProva = new Prova();
			//novaNota = new Nota();
		}

		private void FormNota_Load(object sender, EventArgs e)
		{

		}

		private void btBuscarAluno_Click(object sender, EventArgs e)
		{
			FormBuscaAluno busca = new FormBuscaAluno(alunoSelecionado, this);
			busca.Show();
		}

		private void btBuscarProva_Click(object sender, EventArgs e)
		{
			FormBuscaProva busca = new FormBuscaProva(provaSelecionada, this);
			busca.Show();
		}

		private void btSalvarNota_Click(object sender, EventArgs e)
		{

			double n = Convert.ToDouble(txtNota.Text);
			if (n < 0 || n >10)
			{
				MessageBox.Show("Nota Inválida", "Erro", MessageBoxButtons.OK);
				txtNota.Text = "";
			}
			else
			{
				if (lbl0ou1.Text == "1")
				{
					NotaBD notaBD = new NotaBD();
					List<Nota> notasBusca = notaBD.buscarNotasPorNomeAluno(txtAluno.Text);

					foreach (Nota nota in notasBusca)
					{
						if (nota.getAluno().getIdAluno() == Convert.ToInt16(lbl.Text))
						{
							Aluno a = nota.getAluno();
							Prova p = nota.getProva();

							Nota not = new Nota();
							not.setAluno(a);
							not.setProva(p);
							not.setNota(Convert.ToDouble(txtNota.Text));

							int result = notaBD.editarNota(not);
							if (result == 0)
							{
								MessageBox.Show("Erro ao editar nota");
							}
							else
							{
								MessageBox.Show("Nota editada com sucesso", "Sucesso", MessageBoxButtons.OK);
								limparCampos();
							}
						}
					}
				}
				else if (lbl0ou1.Text == "2")
				{
					NotaBD notaBD = new NotaBD();
					List<Nota> notasBusca = notaBD.buscarNotasPorNomeAluno(txtAluno.Text);

					foreach (Nota nota in notasBusca)
					{
						if (nota.getAluno().getIdAluno() == Convert.ToInt16(lbl.Text))
						{
							Aluno a = nota.getAluno();
							Prova p = nota.getProva();

							Nota not = new Nota();
							not.setAluno(a);
							not.setProva(p);
							not.setNota(Convert.ToDouble(txtNota.Text));

							int result = notaBD.apagarNota(not);
							if (result == 0)
							{
								MessageBox.Show("Erro ao Apagar registro de nota");
							}
							else
							{
								MessageBox.Show("Nota Apagada com sucesso", "Sucesso", MessageBoxButtons.OK);
								limparCampos();
							}
						}
					}
				}
				else
				{
					Nota not = new Nota();

					not.setAluno(alunoSelecionado);
					not.setProva(provaSelecionada);
					not.setNota(Convert.ToDouble(txtNota.Text));

					NotaBD notaBD = new NotaBD();
					//Chama o metodo da classe NotaBD pra verificar no banco se existe algum registro igual ao que está sendo salvo
					int x = notaBD.verificaSeExisteRegistro(not);
					if (x == 0)
					{
						int result = notaBD.inserirNota(not);
						if (result == 0)
						{
							MessageBox.Show("Erro ao registrar nota");
						}
						else
						{
							MessageBox.Show("Nota registrada com sucesso", "Sucesso", MessageBoxButtons.OK);
							limparCampos();
						}
					}
					else
					{
						MessageBox.Show("Esse registro já existe no banco", "Erro", MessageBoxButtons.OK);
						limparCampos();
					}
				}

			}
		}
		public void limparCampos()
		{
			txtAluno.Text = "";
			txtProva.Text = "";
			txtNota.Text = "";
		}

		private void txtNota_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
