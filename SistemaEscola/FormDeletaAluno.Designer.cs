﻿namespace SistemaEscola
{
	partial class FormDeletaAluno
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gridResultadoAluno = new System.Windows.Forms.DataGridView();
			this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Aluno = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Matrícula = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtBuscarAluno = new System.Windows.Forms.TextBox();
			this.btApagarAluno = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoAluno)).BeginInit();
			this.SuspendLayout();
			// 
			// gridResultadoAluno
			// 
			this.gridResultadoAluno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridResultadoAluno.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Aluno,
            this.Matrícula});
			this.gridResultadoAluno.Location = new System.Drawing.Point(3, 62);
			this.gridResultadoAluno.Name = "gridResultadoAluno";
			this.gridResultadoAluno.Size = new System.Drawing.Size(350, 211);
			this.gridResultadoAluno.TabIndex = 3;
			this.gridResultadoAluno.Visible = false;
			this.gridResultadoAluno.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridResultadoAluno_CellContentClick);
			// 
			// ID
			// 
			this.ID.HeaderText = "ID";
			this.ID.Name = "ID";
			this.ID.Width = 25;
			// 
			// Aluno
			// 
			this.Aluno.HeaderText = "Aluno";
			this.Aluno.Name = "Aluno";
			this.Aluno.Width = 220;
			// 
			// Matrícula
			// 
			this.Matrícula.HeaderText = "Matrícula";
			this.Matrícula.Name = "Matrícula";
			this.Matrícula.Width = 62;
			// 
			// txtBuscarAluno
			// 
			this.txtBuscarAluno.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtBuscarAluno.Location = new System.Drawing.Point(60, 12);
			this.txtBuscarAluno.Name = "txtBuscarAluno";
			this.txtBuscarAluno.Size = new System.Drawing.Size(241, 20);
			this.txtBuscarAluno.TabIndex = 2;
			this.txtBuscarAluno.TextChanged += new System.EventHandler(this.txtBuscarAluno_TextChanged);
			// 
			// btApagarAluno
			// 
			this.btApagarAluno.Location = new System.Drawing.Point(88, 300);
			this.btApagarAluno.Name = "btApagarAluno";
			this.btApagarAluno.Size = new System.Drawing.Size(75, 23);
			this.btApagarAluno.TabIndex = 9;
			this.btApagarAluno.Text = "Apagar";
			this.btApagarAluno.UseVisualStyleBackColor = true;
			this.btApagarAluno.Click += new System.EventHandler(this.btApagarAluno_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(197, 300);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 10;
			this.button1.Text = "Cancelar";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// FormDeletaAluno
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(356, 353);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.btApagarAluno);
			this.Controls.Add(this.gridResultadoAluno);
			this.Controls.Add(this.txtBuscarAluno);
			this.Name = "FormDeletaAluno";
			this.Text = "FormDeletaAluno";
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoAluno)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView gridResultadoAluno;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID;
		private System.Windows.Forms.DataGridViewTextBoxColumn Aluno;
		private System.Windows.Forms.DataGridViewTextBoxColumn Matrícula;
		private System.Windows.Forms.TextBox txtBuscarAluno;
		public System.Windows.Forms.Button btApagarAluno;
		public System.Windows.Forms.Button button1;
	}
}