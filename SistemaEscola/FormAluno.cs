﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEscola
{
	public partial class FormAluno : Form
	{
		public FormAluno()
		{
			InitializeComponent();
		}

		private void txtNome_TextChanged(object sender, EventArgs e)
		{

		}

		private void btSalvarAluno_Click(object sender, EventArgs e)
		{
			Aluno a = new Aluno();
			
			//////////////////
			if(lbl0ou1.Text == "1")
			{
				int x = Convert.ToInt16(lblidA.Text);
				a.setIdAluno(x);
				a.setNome(txtNome.Text);
				a.setDataNasc(txtDataNasc.Value);
				a.setMatricula(txtMatricula.Text);
				a.setDataMatricula(txtDataMatricula.Value);
			}
			else
			{
				a.setNome(txtNome.Text);
				a.setDataNasc(txtDataNasc.Value);
				a.setMatricula(txtMatricula.Text);
				a.setDataMatricula(txtDataMatricula.Value);
			}

			//alunos.Add(a);

			AlunoBD alunoBD = new AlunoBD();
			NotaBD notaBD = new NotaBD();

			if (btSalvarAluno.Text == "Apagar")
			{
				Nota n = new Nota();
				n.setAluno(a);
				//int r = notaBD.apagarNota(n);

				//if (r == 0)
				//{
				//	MessageBox.Show("Erro ao apagar cadastro de aluno na Tabela Nota");
				//}
				//else
				//{
					//MessageBox.Show("Cadastro de aluno apagado com sucesso", "Sucesso", MessageBoxButtons.OK);
					//limparCampos();
					int result = alunoBD.apagarAluno(a);
					if (result == 0)
					{
						MessageBox.Show("Erro ao apagar cadastro de aluno na Tabela Aluno");
					}
					else
					{
						MessageBox.Show("Cadastro de aluno apagado com sucesso", "Sucesso", MessageBoxButtons.OK);
						limparCampos();
					}
				//}
				//
				
			}
			////////////
			else if (lbl0ou1.Text == "1")
			{
				//a.setIdAluno(Convert.ToInt16(formMenu.lblid.Text));
				
				int result = alunoBD.editarAluno(a);
				if (result == 0)
				{
					MessageBox.Show("Erro ao editar cadastro de aluno");
				}
				else
				{
					MessageBox.Show("Cadastro de aluno editado com sucesso", "Sucesso", MessageBoxButtons.OK);
					limparCampos();
				}
			}
			else
			{
				int result = alunoBD.inserirAluno(a);
				if (result == 0)
				{
					MessageBox.Show("Erro ao cadastrar aluno");
				}
				else
				{
					MessageBox.Show("Aluno salvo com sucesso", "Sucesso", MessageBoxButtons.OK);
					limparCampos();
				}
			}
		}
		public void limparCampos()
		{
			txtNome.Text = "";
			txtDataNasc.Text = "";
			txtMatricula.Text = "";
			txtDataMatricula.Text = "";
		}

		private void lbl0ou1_Click(object sender, EventArgs e)
		{

		}

		private void FormAluno_Load(object sender, EventArgs e)
		{

		}
	}
}
