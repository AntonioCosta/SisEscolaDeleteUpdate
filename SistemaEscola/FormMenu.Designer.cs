﻿namespace SistemaEscola
{
	partial class FormMenu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editarCadastroAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editarCadastroDeNotaDoAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.disciplinaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.txtAluno = new System.Windows.Forms.TextBox();
			this.lblid = new System.Windows.Forms.Label();
			this.registroDoAlunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.somenteNotaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.disciplinaToolStripMenuItem});
			this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(30, 50, 30, 50);
			this.menuStrip1.Size = new System.Drawing.Size(234, 207);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// alunoToolStripMenuItem
			// 
			this.alunoToolStripMenuItem.AutoSize = false;
			this.alunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarCadastroAlunoToolStripMenuItem,
            this.editarCadastroDeNotaDoAlunoToolStripMenuItem});
			this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
			this.alunoToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.alunoToolStripMenuItem.Size = new System.Drawing.Size(150, 50);
			this.alunoToolStripMenuItem.Text = "Editar Cadastro";
			this.alunoToolStripMenuItem.Click += new System.EventHandler(this.alunoToolStripMenuItem_Click);
			// 
			// editarCadastroAlunoToolStripMenuItem
			// 
			this.editarCadastroAlunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.editarCadastroAlunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.editarCadastroAlunoToolStripMenuItem.Name = "editarCadastroAlunoToolStripMenuItem";
			this.editarCadastroAlunoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.editarCadastroAlunoToolStripMenuItem.Text = "Dados";
			this.editarCadastroAlunoToolStripMenuItem.Click += new System.EventHandler(this.editarCadastroAlunoToolStripMenuItem_Click);
			// 
			// editarCadastroDeNotaDoAlunoToolStripMenuItem
			// 
			this.editarCadastroDeNotaDoAlunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlLight;
			this.editarCadastroDeNotaDoAlunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.editarCadastroDeNotaDoAlunoToolStripMenuItem.Name = "editarCadastroDeNotaDoAlunoToolStripMenuItem";
			this.editarCadastroDeNotaDoAlunoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.editarCadastroDeNotaDoAlunoToolStripMenuItem.Text = "Nota";
			this.editarCadastroDeNotaDoAlunoToolStripMenuItem.Click += new System.EventHandler(this.editarCadastroDeNotaDoAlunoToolStripMenuItem_Click);
			// 
			// disciplinaToolStripMenuItem
			// 
			this.disciplinaToolStripMenuItem.AutoSize = false;
			this.disciplinaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.disciplinaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registroDoAlunoToolStripMenuItem,
            this.somenteNotaToolStripMenuItem});
			this.disciplinaToolStripMenuItem.Name = "disciplinaToolStripMenuItem";
			this.disciplinaToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 10, 4, 10);
			this.disciplinaToolStripMenuItem.Size = new System.Drawing.Size(150, 50);
			this.disciplinaToolStripMenuItem.Text = "Apagar";
			this.disciplinaToolStripMenuItem.Click += new System.EventHandler(this.disciplinaToolStripMenuItem_Click);
			// 
			// txtAluno
			// 
			this.txtAluno.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.txtAluno.Enabled = false;
			this.txtAluno.Location = new System.Drawing.Point(12, 12);
			this.txtAluno.Name = "txtAluno";
			this.txtAluno.Size = new System.Drawing.Size(210, 20);
			this.txtAluno.TabIndex = 6;
			// 
			// lblid
			// 
			this.lblid.AutoSize = true;
			this.lblid.Location = new System.Drawing.Point(13, 29);
			this.lblid.Name = "lblid";
			this.lblid.Size = new System.Drawing.Size(35, 13);
			this.lblid.TabIndex = 7;
			this.lblid.Text = "label1";
			this.lblid.Visible = false;
			// 
			// registroDoAlunoToolStripMenuItem
			// 
			this.registroDoAlunoToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.registroDoAlunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.registroDoAlunoToolStripMenuItem.Name = "registroDoAlunoToolStripMenuItem";
			this.registroDoAlunoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.registroDoAlunoToolStripMenuItem.Text = "Registro do Aluno";
			this.registroDoAlunoToolStripMenuItem.Click += new System.EventHandler(this.registroDoAlunoToolStripMenuItem_Click);
			// 
			// somenteNotaToolStripMenuItem
			// 
			this.somenteNotaToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.somenteNotaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.somenteNotaToolStripMenuItem.Name = "somenteNotaToolStripMenuItem";
			this.somenteNotaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.somenteNotaToolStripMenuItem.Text = "Somente Nota";
			this.somenteNotaToolStripMenuItem.Click += new System.EventHandler(this.somenteNotaToolStripMenuItem_Click);
			// 
			// FormMenu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(234, 207);
			this.Controls.Add(this.lblid);
			this.Controls.Add(this.txtAluno);
			this.Controls.Add(this.menuStrip1);
			this.Name = "FormMenu";
			this.Text = "Menu";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editarCadastroAlunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem disciplinaToolStripMenuItem;
		public System.Windows.Forms.TextBox txtAluno;
		public System.Windows.Forms.Label lblid;
		private System.Windows.Forms.ToolStripMenuItem editarCadastroDeNotaDoAlunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem registroDoAlunoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem somenteNotaToolStripMenuItem;
	}
}