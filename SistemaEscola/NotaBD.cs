﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	class NotaBD
	{
		public int inserirNota(Nota nota)
		{
			/*Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();

			NpgsqlCommand cmdTest = new NpgsqlCommand("select id_aluno, id_prova from aluno, prova where nome ='" + nota.getAluno().getNome() + "'", conn);

			NpgsqlCommand cmd = new NpgsqlCommand("insert into nota(id_aluno, id_prova from aluno, valor_nota) " +
				"values ('" + nota.getAluno().getIdAluno() + "','" + nota.getProva().getIdProva() + "','" + nota.getNota() + "')", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;*/

			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			//int n = nota.getAluno().getIdAluno();
			NpgsqlCommand cmd = new NpgsqlCommand("insert into nota(id_aluno, id_prova, valor_nota) " +
				"values ('" + nota.getAluno().getIdAluno() + "','" + nota.getProva().getIdProva() + "','" + nota.getNota() + "')", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;
		}

		public int editarNota(Nota nota)
		{
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("update nota set valor_nota = " + nota.getNota() + " " +
				"where id_aluno = " + nota.getAluno().getIdAluno() + " and id_prova = " + nota.getProva().getIdProva() + " ", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;
		}

		public int apagarNota(Nota nota)
		{
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("delete from nota where id_aluno = '" + nota.getAluno().getIdAluno() + "' ", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;
		}

		public List<Nota> buscarNotasPorNomeAluno(String nomeAluno)
		{
			
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("select * from aluno as a, prova as p, nota as n " +
				"where a.nome like '%" + nomeAluno + "%' and a.id = n.id_aluno and p.id = n.id_prova", conn);
			NpgsqlDataReader dRead = cmd.ExecuteReader();
			List<Nota> notas = new List<Nota>();

			while (dRead.Read())
			{
				Aluno a = new Aluno();
				Prova p = new Prova();
				Nota n = new Nota();
				
				a.setIdAluno(Convert.ToInt16(dRead[0]));
				a.setNome(Convert.ToString(dRead[1]));
				a.setMatricula(Convert.ToString(dRead[2]));
				a.setDataNasc(Convert.ToDateTime(dRead[3]));
				a.setDataMatricula(Convert.ToDateTime(dRead[4]));

				p.setIdProva(Convert.ToInt32(dRead[5]));
				p.setDataProva(Convert.ToDateTime(dRead[6]));
				p.setDescricao(Convert.ToString(dRead[7]));

				n.setAluno(a);
				n.setProva(p);
				n.setNota(Convert.ToDouble(dRead[10]));
				
				notas.Add(n);
			}
			c.desconecta();
			return notas;
		}

		public List<Nota> buscarNotasPorDescricao(String descricao, DateTime data)
		{

			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("select * from aluno as a, prova as p, nota as n " +
				"where p.descricao = '" + descricao + "' and p.data_realizacao = '" + data + "' and a.id = n.id_aluno and p.id = n.id_prova", conn);
			NpgsqlDataReader dRead = cmd.ExecuteReader();
			List<Nota> notas = new List<Nota>();

			while (dRead.Read())
			{
				Aluno a = new Aluno();
				Prova p = new Prova();
				Nota n = new Nota();

				a.setIdAluno(Convert.ToInt16(dRead[0]));
				a.setNome(Convert.ToString(dRead[1]));
				a.setMatricula(Convert.ToString(dRead[2]));
				a.setDataNasc(Convert.ToDateTime(dRead[3]));
				a.setDataMatricula(Convert.ToDateTime(dRead[4]));

				p.setIdProva(Convert.ToInt32(dRead[5]));
				p.setDataProva(Convert.ToDateTime(dRead[6]));
				p.setDescricao(Convert.ToString(dRead[7]));

				n.setAluno(a);
				n.setProva(p);
				n.setNota(Convert.ToDouble(dRead[10]));

				notas.Add(n);
			}
			c.desconecta();
			return notas;
		}

		//Serve pra verificar no banco se existe algum registro igual ao que está sendo salvo
		public int verificaSeExisteRegistro(Nota nota)
		{
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("select count(a.nome) from aluno as a, prova as p, nota as n " +
				"where a.id = '" + nota.getAluno().getIdAluno() + "' and p.id = '" + nota.getProva().getIdProva() + "' " +
				"and a.id = n.id_aluno and p.id = n.id_prova", conn);
			NpgsqlDataReader dRead = cmd.ExecuteReader();
			//List<Nota> n = new List<Nota>();

			int x = 0;
			while (dRead.Read())
			{
				x = Convert.ToInt16(dRead[0]);
				if(x != 0)
				{
					c.desconecta();
					return x;
				}
			}
			c.desconecta();
			return x;
		}
	}
}
